require('dotenv').config();

// fill in the .env file in the root directory like this: DB_PASSWORD=123
module.exports = {
	development: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_DATABASE,
		host: '127.0.0.1',
		dialect: process.env.DB_DIALECT,
		logging: false,
		define: {
			timestamps: false,
			freezeTableName: true
		}
	},
	test: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_TEST,
		host: '127.0.0.1',
		dialect: process.env.DB_DIALECT,
		logging: false,
		define: {
			timestamps: false,
			freezeTableName: true
		}
	}
};