'use strict';

process.env.NODE_ENV = 'test';
const fs = require('fs');
const path = require('path');
const { describe, before, it } = require('mocha');
const { expect } = require('chai');
const models = {};
const modelsMock = {};
const SequelizeMock = require('sequelize-mock');
const sequelizeMock = new SequelizeMock();

const {
	sequelize,
	dataTypes,
	checkModelName,
	checkPropertyExists
} = require('sequelize-test-helpers');

const modelsDirName = './db/models/';
fs
	.readdirSync(modelsDirName)
	.filter(file => {
		return (file.indexOf('.') !== 0) && (file !== 'index.js') && (file.slice(-3) === '.js');
	})
	.forEach(file => {
		const model = require(path.join('..', modelsDirName, file))(sequelize, dataTypes);
		models[model.modelName] = model;

		const modelMock = require(path.join('..', modelsDirName, file))(sequelizeMock, SequelizeMock);
		modelsMock[modelMock.name] = modelMock;
	});

describe('db/models/User', () => {
	const userInstance = new models.User();
    
	checkModelName(models.User)('User');

	describe('properties', () => {
		['id', 'nickname', 'password'].forEach(checkPropertyExists(userInstance));
	});
});

describe('db/models/RoundResults', () => {
	const roundResultsInstance = new models.RoundResults();
    
	checkModelName(models.RoundResults)('RoundResults');

	describe('properties', () => {
		['id', 'winningTeam', 'endTime'].forEach(checkPropertyExists(roundResultsInstance));
	});
});

describe('db/models/UserResults', () => {
	const userResultsInstance = new models.UserResults();
    
	checkModelName(models.UserResults)('UserResults');

	describe('properties', () => {
		['score', 'incidentAccuracy', 'incidentCount', 'side', 'winner'].forEach(checkPropertyExists(userResultsInstance));
	});
});

describe('associations', () => {
	before(() => {
		Object.keys(models).forEach(modelName => {
			if('associate' in models[modelName]) {
				models[modelName].associate(models);
			}
		});
	});

	it('defined a belongsToMany association with User as friends through Friends', () => {
		expect(models.User.belongsToMany).to.have.been.calledWith(models.User, { 
			as: 'friends',
			through: 'Friends'
		});
	});

	it('defined a belongsToMany association with User as requestee through FriendRequests', () => {
		expect(models.User.belongsToMany).to.have.been.calledWith(models.User, { 
			as: 'requestee',
			through: 'FriendRequests',
			foreignKey: 'requesterId',
			onDelete: 'CASCADE' 
		});
	});

	it('defined a belongsToMany association with User as requesters through FriendRequests', () => {
		expect(models.User.belongsToMany).to.have.been.calledWith(models.User, { 
			as: 'requesters',
			through: 'FriendRequests',
			foreignKey: 'requesteeId',
			onDelete: 'CASCADE' 
		});
	});

	it('defined a belongsToMany association with RoundResults through UserResults', () => {
		expect(models.User.belongsToMany).to.have.been.calledWith(models.RoundResults, { 
			through: 'UserResults' });
	});

	it('defined a belongsToMany association with User as friends through Friends', () => {
		expect(models.RoundResults.belongsToMany).to.have.been.calledWith(models.User, { 
			through: 'UserResults'
		});
	});
});