process.env.NODE_ENV = 'test';

const { describe, it, /*before*/ } = require('mocha');
const server = require('../bin/www');
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
// const db = require('../db/models/index');

chai.use(chaiHttp);

describe('app', () => {
	describe('error handling', () => {
		it('should return index page', (done) => {
			chai.request(server)
				.get('/11')
				.then(res => {
					expect(res).to.have.status(404);
					done();
				});
		});
	});
	
	describe('GET /', () => {
		it('should return index page', (done) => {
			chai.request(server)
				.get('/')
				.then(res => {
					expect(res).to.have.status(200);
					done();
				});
		});
	});

	describe('GET /signup', () => {
		it('should return sign up form page', (done) => {
			chai.request(server)
				.get('/signup')
				.then(res => {
					expect(res).to.have.status(200);
					done();
				});
		});
	});

	describe('POST /signup', () => {
		// doesn't work with gitlab ci yet
		before((done) => {
			db.User.destroy({
				truncate: { cascade: true }
			}).then(done());
		});

		const user = {
			'nickname' : 'superuser',
			'password': 'qwerty'
		};

		it('should post signup form', (done) => {
			chai.request(server)
				.post('/signup')
				.type('form')
				.send(user).then(res => {
					expect(res).to.have.status(200);
					done();
				});
		});
		
		// doesn't work with gitlab ci yet
		it('should exist user model', () => {
			db.User.findOne().then((user) => {
				expect(user.nickname).to.equal(user.nickname);
			});
		});
	});

	describe('GET /signin', () => {
		it('should return sign in form page', (done) => {
			chai.request(server)
				.get('/signin')
				.then(res => {
					expect(res).to.have.status(200);
					done();
				});
		});
	});

	const user = {
		'nickname' : 'superuser',
		'password': 'qwerty'
	};

	describe('POST /signin', () => {
		it('should be succesfull login', (done) => {
			chai.request(server)
				.post('/signin')
				.type('form')
				.send(user).then(res => {
					expect(res).to.have.status(200);
					done();
				});
		});
	});
	

	describe('GET /logout', () => {
		it('should redirect to /', (done) => {
			chai.request(server)
				.get('/logout')
				.then(res => {
					expect(res).to.have.status(200);
					done();
				});
		});
	});
});