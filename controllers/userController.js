const { body, validationResult } = require('express-validator');
const passport = require('passport');
// const { sanitizeBody } = require('express-validator');
const db = require('../db/models/index');

require('../config/passport')(passport);

module.exports.userCreateGet = (req, res) => {
	res.render('auth', { title: 'Sign up' });
};

module.exports.postValidate = [
	body('nickname').trim().isLength({ min: 4 }).escape().withMessage('Minimum login length 4 characters.')
		.isAlphanumeric().withMessage('Login has non-alphanumeric characters.')
		.custom(value => {
			return db.User.findOne({ where: { nickname: value } }).then((user) => {
				if (user) {
					return Promise.reject('This nickname is already exists.');
				}
			});
		}),
	body('password').trim().isLength({ min: 6 }).escape().withMessage('Minimum password length 6 characters.'),

	async (req, res, next) => {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			res.render('auth', { title: 'Sign up', errors: errors.array() });
			return;
		}
		next();
	}
];

module.exports.userSignupPost = passport
	.authenticate('local-signup'), (req, res) => {
	res.redirect('/');
};

module.exports.userLoginGet = (req, res) => {
	res.render('auth', { title: 'Sign in' });
};

module.exports.userSigninPost = passport
	.authenticate('local-signin'), (req, res) => {
	res.redirect('/');
};

module.exports.userLogoutGet = (req, res) => {
	req.session.destroy((err) => {
		if (err) {
			// log here
		}
		res.redirect('/');
	});
};