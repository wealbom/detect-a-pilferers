'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		return await queryInterface.createTable('RoundResults', {
			id: {
				type: Sequelize.INTEGER(11),
				autoIncrement: true,
				primaryKey: true,
				allowNull: false
			},
			winningTeam: {
				type: Sequelize.BOOLEAN,
				allowNull: false
			},
			endTime: {
				type: Sequelize.DATE,
				allowNull: false
			}
		});
	},

	down: async (queryInterface) => {
		return await queryInterface.dropTable('RoundResults');
	}
};