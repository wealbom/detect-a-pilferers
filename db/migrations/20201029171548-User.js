'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		return await queryInterface.createTable('User', {
			id: {
				type: Sequelize.INTEGER(11),
				autoIncrement: true,
				primaryKey: true,
				allowNull: false
			},
			nickname: {
				type: Sequelize.STRING(32),
				allowNull: false,
				unique: true,
			},
			password: {
				type: Sequelize.STRING(256),
				allowNull: false,
			}
		});
	},

	down: async (queryInterface) => {
		return await queryInterface.dropAllTables();
	}
};
