'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		return await queryInterface.createTable('Friends', {
			userId: {
				type: Sequelize.INTEGER(11),
				allowNull: false,
				references: {
					model: {
						tableName: 'User',
					},
					key: 'id'
				},
				onDelete: 'CASCADE'
			},
			friendId: {
				type: Sequelize.INTEGER(11),
				allowNull: false,
				references: {
					model: {
						tableName: 'User',
					},
					key: 'id'
				},
				onDelete: 'CASCADE'
			}
		});
	},

	down: async (queryInterface) => {
		return await queryInterface.dropTable('Friends');
	}
};