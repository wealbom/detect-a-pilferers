'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		return await queryInterface.createTable('UserResults', {
			score: {
				type: Sequelize.INTEGER(6),
				allowNull: false
			},
			incidentAccuracy:{
				type: Sequelize.INTEGER(3),
				allowNull: false
			},
			incidentCount:{
				type: Sequelize.INTEGER(3),
				allowNull: false
			},
			side: {
				type: Sequelize.BOOLEAN,
				allowNull: false
			},
			winner: {
				type: Sequelize.BOOLEAN,
				allowNull: false
			},
			roundResultId: {
				type: Sequelize.INTEGER(11),
				allowNull: false,
				references: {
					model: {
						tableName: 'RoundResults',
					},
					key: 'id'
				},
				onDelete: 'CASCADE'
			},
			userId: {
				type: Sequelize.INTEGER(11),
				allowNull: false,
				references: {
					model: {
						tableName: 'User',
					},
					key: 'id'
				},
				onDelete: 'CASCADE'
			}
		});
	},

	down: async (queryInterface) => {
		return await queryInterface.dropTable('UserResults');
	}
};