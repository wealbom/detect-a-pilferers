module.exports = (sequelize, DataTypes) => {
	const UserResults = sequelize.define('UserResults', {
		score: {
			type: DataTypes.INTEGER(6),
			allowNull: false
		},
		incidentAccuracy:{
			type: DataTypes.INTEGER(3),
			allowNull: false
		// for catchers: accuracy of successful theft prevention
		// for thieves: accuracy of successful theft or escape
		},
		incidentCount:{
			type: DataTypes.INTEGER(3),
			allowNull: false
		},
		side: {
			type: DataTypes.BOOLEAN,
			allowNull: false
		},
		winner: {
			type: DataTypes.BOOLEAN,
			allowNull: false
		}
	});
	return UserResults;
};