const crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
		},
		nickname: {
			type: DataTypes.STRING(32),
			allowNull: false,
			unique: true,
		},
		password: {
			type: DataTypes.STRING(256),
			allowNull: false,
			set(value) {
				this.setDataValue('password', encryptPassword(value, this.nickname));
			}
		}
	});
	User.associate = (models) => {
		// many-to-many association for friends list
		User.belongsToMany(User, { as: 'friends', through: 'Friends' });

		// many-to-many associations for a list of friend requests
		User.belongsToMany(User, { as: 'requestee', through: 'FriendRequests', foreignKey: 'requesterId', onDelete: 'CASCADE' });
		User.belongsToMany(User, { as: 'requesters', through: 'FriendRequests', foreignKey: 'requesteeId', onDelete: 'CASCADE' });

		// many-to-many associations for the list of users who participated in the round
		User.belongsToMany(models.RoundResults, { through: 'UserResults' });
	};
	
	return User;
};

function encryptPassword(password,salt) {
	return crypto.createHmac('sha256', salt).update(password).digest('hex');
}